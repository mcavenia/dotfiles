execute pathogen#infect()
colorscheme solarized
syntax on
filetype plugin indent on
set tabstop=4  "number of spaces visually shown per tab
set softtabstop=4 "actual number of spaces written per tab
set expandtab "tabs are spaces
set number "line numbers
set cursorline
set wildmenu "graphical autocomplete
set showmatch "matching braces, parens
set incsearch "search as you type
set hlsearch "highlight matches
set laststatus=2 "patch for lightline
set noshowmode "removes vim status bar


set foldenable "folding
set foldlevelstart=10 "by default nearly all folds open
set foldnestmax=10

"space opens/closes current fold
nnoremap <silent> <space> za

set foldmethod=indent
let mapleader="," "sets comma to leader

"leader and u execute gundo
nnoremap <leader>u :GundoToggle<CR>

"leader and s executes save session, reopen using vim -S
nnoremap <leader>s :mksession<CR>

"f5  redraws with no highlighting
nnoremap <silent> <F5> :nohl<CR><F5>

"lightline configuration
let g:lightline = {
      \ 'colorscheme': 'solarized_dark',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'fugitive', 'filename' ] ]
      \ },
      \ 'component_function': {
      \   'fugitive': 'LightLineFugitive',
      \   'readonly': 'LightLineReadonly',
      \   'modified': 'LightLineModified',
      \   'filename': 'LightLineFilename'
      \ },
      \ 'separator': { 'left': "\ue0b0", 'right': "\ue0b2" },
      \ 'subseparator': { 'left': "\ue0b1", 'right': "\ue0b3" }
      \ }

    function! LightLineModified()
            if &filetype == "help"
                return ""
            elseif &modified
                return "+"
            elseif &modifiable
                return ""
            else
                return ""
            endif
    endfunction

    function! LightLineReadonly()
        if &filetype == "help"
            return ""
        elseif &readonly
            return "⭤"
        else
            return ""
        endif
    endfunction
    
    function! LightLineFugitive()
        if exists("*fugitive#head")
            let _ = fugitive#head()
            return strlen(_) ? '⭠ '._ : ''
        endif
        return ''
    endfunction  
    
    function! LightLineFilename()
              return ('' != LightLineReadonly() ? LightLineReadonly() . ' ' : '') .
                     \ ('' != expand('%:t') ? expand('%:t') : '[No Name]') .
                     \ ('' != LightLineModified() ? ' ' . LightLineModified() : '')
    endfunction
